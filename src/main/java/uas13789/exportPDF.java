package uas13789;

import java.sql.Connection;
import java.util.HashMap;
import uas13789.models.connection;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class exportPDF {
    private Connection conn;

    public void export() {
        try {
            connection connection = new connection();
            connection.KoneksiMySQL();
            conn = connection.getCon();
            JasperReport jasperReport = JasperCompileManager.compileReport("src/main/resources/Invoice.jrxml");

            // Mengisi laporan dengan data dari database
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<>(), conn);

            // Export laporan ke file PDF
            String outputFilePath = "laporan/uas13789.pdf";
            JasperExportManager.exportReportToPdfFile(jasperPrint, outputFilePath);

            System.out.println("Laporan PDF berhasil diekspor ke " + outputFilePath);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
