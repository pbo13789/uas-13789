package uas13789.models;

import java.sql.*;

public class connection {

    public Connection con;
    public Statement stm;
    private ResultSet activeAdmin;

    public void KoneksiMySQL() {
        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            String DB_URL = "jdbc:mysql://localhost:3306/uas13789";
            String USER = "root";
            String PASS = "";

            con = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
            stm = con.createStatement();
            System.out.println("Koneksi Berhasil");
        } catch (Exception e) {
            System.out.println("Koneksi Gagal");
        }
    }

    public void getActive() {
        try {
            String query = "SELECT active FROM user";
            activeAdmin = stm.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet getActiveResult() {
        return activeAdmin;
    }

    public Connection getCon() {
        return con;
    }

    public Statement getStm() {
        return stm;
    }
}
